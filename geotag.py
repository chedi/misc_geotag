import os
import sys
import pyexiv2

from PIL          import Image
from fractions    import Fraction
from PIL.ExifTags import TAGS

def to_deg(value, loc):
    if value < 0:
        loc_value = loc[0]
    elif value > 0:
        loc_value = loc[1]
    else:
        loc_value = ""
    abs_value = abs(value)
    deg =  int(abs_value)
    t1 = (abs_value-deg)*60
    min = int(t1)
    sec = round((t1 - min)* 60, 5)
    return (deg, min, sec, loc_value)

def set_gps_location(file_name, lat, lng):
    lat_deg = to_deg(lat, ["S", "N"])
    lng_deg = to_deg(lng, ["W", "E"])

    print lat_deg
    print lng_deg

    # convert decimal coordinates into degrees, munutes and seconds
    exiv_lat = (pyexiv2.Rational(lat_deg[0]*60+lat_deg[1],60),pyexiv2.Rational(lat_deg[2]*100,6000), pyexiv2.Rational(0, 1))
    exiv_lng = (pyexiv2.Rational(lng_deg[0]*60+lng_deg[1],60),pyexiv2.Rational(lng_deg[2]*100,6000), pyexiv2.Rational(0, 1))

    exiv_image = pyexiv2.ImageMetadata(file_name)
    exiv_image.read()
    exif_keys = exiv_image.exif_keys 

    exiv_image["Exif.GPSInfo.GPSLatitude"    ] = exiv_lat
    exiv_image["Exif.GPSInfo.GPSLatitudeRef" ] = lat_deg[3]
    exiv_image["Exif.GPSInfo.GPSLongitude"   ] = exiv_lng
    exiv_image["Exif.GPSInfo.GPSLongitudeRef"] = lng_deg[3]
    exiv_image["Exif.Image.GPSTag"           ] = 654
    exiv_image["Exif.GPSInfo.GPSMapDatum"    ] = "WGS-84"
    exiv_image["Exif.GPSInfo.GPSVersionID"   ] = '2 0 0 0'

    exiv_image.write()


def extract_image(video_file_name, seek_time, resolution, extracted_name):
    command = "ffmpeg -i %s -ss %s -t 00:00:1 -s %s -r 1 -vframes 1 -y %s" % (video_file_name, seek_time, resolution, extracted_name)
    os.system(command)

if len(sys.argv) == 5 and sys.argv[1] == 'w':
    set_gps_location(sys.argv[2], float(sys.argv[3]), float(sys.argv[4]))
else:
    print("usage w file_path lat lon")